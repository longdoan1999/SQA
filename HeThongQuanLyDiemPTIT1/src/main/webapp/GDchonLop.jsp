<%-- 
    Document   : GDchonmon
    Created on : Apr 14, 2021, 8:59:00 PM
    Author     : Cao Sy Hai Long
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Lop"%>
<%@page import="controller.DataController"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Danh sách lớp</title>
        <style>
            table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
            }
        </style>
    </head>
    <body>
        <%
            String maMon = request.getParameter("MonHoc");
            System.out.println("Ma mon: " + maMon);
            DataController control = new DataController();
            ArrayList<Lop> cacLop = control.getLopByMaMon(maMon);
            System.out.println("So nhom: " + cacLop.size());
            
        %>
        <h1 style="text-align: center">Danh sách lớp</h1>
        <form action="GDdiem.jsp" method="get">
            <input type="hidden" id="monID" name="maMon" value=<%= maMon %>>
            <table style="margin-left: auto; margin-right: auto;">
                <tr>
                    <th>Tên nhóm</th>
                    <th>Mã nhóm</th>
                    <th>Kíp học</th>
                    <th>Thứ</th>
                </tr>
                <%
                    for(int i = 0; i < cacLop.size(); i++) {
                        Lop lop = cacLop.get(i);
                        String tenNhom = "Nhóm " + lop.getNhom();
                        
                %>
                <tr>
                    <td><%= tenNhom %></td>
                    <td><%= lop.getMaLop() %></td>
                    <td><%= lop.getKipHoc() %></td>
                    <td><%= lop.getThu() %></td>
                    <td>
                        <Button type="Submit" name="maLop" value= <%= lop.getMaLop() %>>Chọn</Button>
                    </td>
                </tr>
                <%
                    }
                %>
            </table>
        </form>
            
        
    </body>
</html>
