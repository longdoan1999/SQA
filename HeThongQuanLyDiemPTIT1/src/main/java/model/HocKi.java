package model;

import java.util.Vector;

public class HocKi {
	private String _maHocKy;
	private int _kyHoc;
	public NamHoc _namHoc;
	public Vector<KiHocMonHoc> _kiHocMonHoc = new Vector<KiHocMonHoc>();

	public HocKi() {
		throw new UnsupportedOperationException();
	}

	public String getMaHocKy() {
		return this._maHocKy;
	}

	public void setMaHocKy(String aMaHocKy) {
		this._maHocKy = aMaHocKy;
	}

	public int getKyHoc() {
		return this._kyHoc;
	}

	public void setKyHoc(int aKyHoc) {
		this._kyHoc = aKyHoc;
	}
}