package controller;

import org.junit.*;

import java.sql.*;

import static org.junit.jupiter.api.Assertions.*;

public class LoginControllerTest {
    @Test
    public void checkEmptyTest() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.checkEmpty("","");
        assertFalse(rs);
    }
    @Test
    public void checkEmpty_UserNameEmpty_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.checkEmpty("","21312");
        assertFalse(rs);
    }
    @Test
    public void checkEmpty_PasswordEmpty_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.checkEmpty("asdwd","");
        assertFalse(rs);
    }
    @Test
    public void verify_WrongUserName_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.verify("longch","abc123","TK001");
        assertFalse(rs);
    }
    @Test
    public void verify_WrongPassword_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.verify("longcsh","abc1234","TK001");
        assertFalse(rs);
    }
    @Test
    public void verify_Wrong_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.verify("longch","abc1233","TK001");
        assertFalse(rs);
    }
    @Test
    public void verify_Correct_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        boolean rs = lg.verify("longcsh","abc123","TK001");
        assertTrue(rs);
    }
    @Test
    public void getMaGVTest() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        lg.setAccountID("TK001");
        String test = "GV001";
        assertEquals(test,lg.getMaGV(lg.getAccountID()));
    }
    @Test
    public void getMaGV_MaGVNull_Test() throws SQLException, ClassNotFoundException {
        LoginController lg = new LoginController();
        lg.setAccountID("TK00");
        assertNull(lg.getMaGV(lg.getAccountID()));
    }
}