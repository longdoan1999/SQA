/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Cao Sy Hai Long
 */
public class LoginController {
    private String hostName;
    private String dbName;
    private String userName;
    private String password;
    private Connection con;
    private String accountID;

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public LoginController() throws SQLException, ClassNotFoundException {
        this.hostName = "localhost";
        this.dbName = "quanlydiemptit";
        this.userName = "root";
        this.password = "jkluio789";
        getMySQLConnection();
    }

    
    public LoginController(String hostName, String dbName, String userName, String password) throws SQLException, ClassNotFoundException {
        this.hostName = hostName;
        this.dbName = dbName;
        this.userName = userName;
        this.password = password;
        getMySQLConnection();
    }
    
    public void getMySQLConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?useSSL=false&allowPublicKeyRetrieval=true";
        con = DriverManager.getConnection(connectionURL, userName, password);
    }
    public boolean checkEmpty(String loginName,String loginPassword){
        if(loginName.toString().trim()==""||loginPassword.toString().trim()==""){
            return false;
        }
        else
            return true;
    }
    public boolean verify(String loginName, String loginPassword, String ID) throws SQLException {
        Statement statement = con.createStatement();
        String sql = "select *\n" +
                     "from taikhoan\n" +
                     "where TenTK =  \"" + loginName + "\" and MatKhau = \"" + loginPassword + "\"";
        ResultSet rs = statement.executeQuery(sql);
        if(rs.next() == false) {
            con.close();
            return false;
        }
        else {
            System.out.println(rs.getString(1));
            System.out.println(rs.getString(2));
            ID = rs.getString(1);
            setAccountID(ID);
            return true;
        }
    }
    
    public String getMaGV(String accountID) throws SQLException {
        Statement statement = con.createStatement();
        accountID = this.accountID;
        String sql = "select maGV\n" +
                     "from giangvien\n" +
                     "where TaiKhoanMaTK =  \"" + accountID + "\"";
        System.out.println("ID tim giang vien: " + accountID);
        ResultSet rs = statement.executeQuery(sql);
        if (rs.next()) {
            String maGV = rs.getString(1);
            con.close();
            return maGV;
        }
        else {
            con.close();
            return null;
        }
    } 
    
    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
