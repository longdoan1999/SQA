package model;

public class TaiKhoan {
	private String _maTK;
	private String _tenTK;
	private String _matKhau;
	public GiangVien _giangVien;

	public TaiKhoan() {
		throw new UnsupportedOperationException();
	}

	public String getMaTK() {
		return this._maTK;
	}

	public void setMaTK(String aMaTK) {
		this._maTK = aMaTK;
	}

	public String getTenTK() {
		return this._tenTK;
	}

	public void setTenTK(String aTenTK) {
		this._tenTK = aTenTK;
	}

	public String getMatKhau() {
		return this._matKhau;
	}

	public void setMatKhau(String aMatKhau) {
		this._matKhau = aMatKhau;
	}
}