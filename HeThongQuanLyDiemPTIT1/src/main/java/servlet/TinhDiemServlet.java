/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import controller.DiemController;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Diem;
import model.SinhVien;

/**
 *
 * @author Cao Sy Hai Long <your.name at your.org>
 */
public class TinhDiemServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        ArrayList<SinhVien> dsSinhVien = (ArrayList<SinhVien>) session.getAttribute("danhSachSV");
        ArrayList<Diem> dsDiem = (ArrayList<Diem>) session.getAttribute("danhSachDiem");
        // He so diem thanh phan
        float cc = Float.parseFloat(request.getParameter("trongSo_cc")) / 100;
        float kt = Float.parseFloat(request.getParameter("trongSo_kt")) / 100;
        float tn_th = Float.parseFloat(request.getParameter("trongSo_tn")) / 100;
        float cuoiKi = Float.parseFloat(request.getParameter("trongSo_ck")) / 100;
        DiemController controller = new DiemController();
        for(int i = 0; i < dsSinhVien.size(); i++) {
            float diemTrungBinh = controller.tinhDiemTrungBinh(cc, kt, tn_th, cuoiKi, dsDiem.get(i));
            dsDiem.get(i).setDiemTrungBinh(diemTrungBinh);
        }

        request.setAttribute("dsSinhVien", dsSinhVien);
        request.setAttribute("dsDiem", dsDiem);
        request.setAttribute("coDiemTrungBinh", Boolean.TRUE);
        String url = "/GDdiem.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
