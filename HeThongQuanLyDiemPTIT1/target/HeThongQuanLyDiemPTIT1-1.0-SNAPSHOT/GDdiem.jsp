<%-- 
    Document   : GDdiem
    Created on : Apr 15, 2021, 2:49:27 PM
    Author     : Cao Sy Hai Long
--%>

<%@page import="controller.DiemController"%>
<%@page import="model.Diem"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.SinhVien"%>
<%@page import="model.MonHoc"%>
<%@page import="controller.DataController"%>
<%@page import="model.Lop"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Danh sách điểm sinh viên</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<%
    //Kiểm tra thông tin mã lớp nhận từ request của GDchonLop.jsp
    String maLop = request.getParameter("maLop");
    System.out.println("Thong tin lop:");
    System.out.println(maLop);
    //Lấy thông tin lớp từ CSDL bằng mã lớp
    DataController control = new DataController();
    DiemController diemController = new DiemController();
    Lop lop = control.getLopByMaLop(maLop);
    //Kiểm tra mã môn nhận từ request của GDchonLop.jsp
    String maMon = request.getParameter("maMon");
    MonHoc monHoc = control.getMonHocByMaMon(maMon);
    String tenMon = monHoc.getTenMon();
    System.out.println(tenMon);
    //Tiêu đề cho thông tin lớp
    String h2_title = "Môn: " + tenMon + " - Nhóm " + lop.getNhom();
    String h3_title = "Sĩ số: " + lop.getSoLuong();
    ArrayList<SinhVien> dsSinhVien = null;
    ArrayList<Diem> dsDiem = null;
%>
<h1 style="text-align: center">Danh sách sinh viên</h1>
<h2 style="text-align: center"><%= h2_title %></h2>
<h3 style="text-align: center"><%= h3_title %></h3>

<div style = "margin: auto">
    <form action="nhapFileExcelServlet" method = "post" enctype="multipart/form-data">
        <input type="hidden" id="lopID" name="maLop" value= <%= maLop %> />
        <input type="hidden" id="monID" name="maMon" value= <%= maMon %> />
        <label for="fileID">Chọn file Excel danh sách điểm:</label>
        <input type="file" id="fileID" accept=".xls, .xlsx" name="excel"><br>
        <button type="submit">Nhập</button>
    </form>
</div>

<%
    Boolean isValid = (Boolean)request.getAttribute("isValid"); // Kiểm tra định dạng file có hợp lệ không
    Boolean coDiemTrungBinh = (Boolean)request.getAttribute("coDiemTrungBinh");
    if(isValid != null) {
        if(isValid.booleanValue() == false) {
%>
<p style="color: red">Sai định dạng file, mời nhập lại</p>
<%
            //Lấy danh sách sinh viên từ CSDL bằng mã lớp
            dsSinhVien = control.getSinhVienByMaLop(maLop);
        }
        else {
            // Lấy danh sách điểm và thông tin sinh viên từ NhapFileExcelServlet
            dsSinhVien = (ArrayList<SinhVien>)request.getAttribute("dsSinhVien");
            dsDiem = (ArrayList<Diem>)request.getAttribute("dsDiem");
        }
    }
    else if(coDiemTrungBinh != null) {
        if(coDiemTrungBinh.booleanValue() == true) {
            dsSinhVien = (ArrayList<SinhVien>)request.getAttribute("dsSinhVien");
            dsDiem = (ArrayList<Diem>)request.getAttribute("dsDiem");
        }
    }
    else
        dsSinhVien = control.getSinhVienByMaLop(maLop);

    if(dsDiem != null) {
        session.setAttribute("danhSachSV", dsSinhVien);
        session.setAttribute("danhSachDiem", dsDiem);
    }

%>

<div>
    <p>Nhập trọng số cho các điểm thành phần (đơn vị: %): </p>
    <form id="form_tinh_diem" action="tinhDiemServlet" method="post">
        <input type="hidden" id="lopID" name="maLop" value= <%= maLop %> />
        <input type="hidden" id="monID" name="maMon" value= <%= maMon %> />
        <label for="cc_ID">CC:</label>
        <input type="number" id="cc_ID" name="trongSo_cc" min="0" max="100" value="0"/>
        <label for="kt_ID">KT:</label>
        <input type="number" id="kt_ID" name="trongSo_kt" min="0" max="100" value="0"/>
        <label for="tn_ID">TN-TH:</label>
        <input type="number" id="tn_ID" name="trongSo_tn" min="0" max="100" value="0"/>
        <label for="ck_ID">Cuối kỳ:</label>
        <input type="number" id="ck_ID" name="trongSo_ck" min="0" max="100" value="0"/>
        <button type="submit">Tính điểm</button>
    </form>
</div>
<form method="post" action="luuDiemLopServlet">
    <input type="hidden" id="lopID" name="maLop" value= <%= maLop %> />
    <input type="hidden" id="monID" name="maMon" value= <%= maMon %> />
    <button>Lưu điểm</button>
    <%
        Boolean daLuu = (Boolean)request.getAttribute("daLuu");
        if(daLuu != null && daLuu.booleanValue() == true) {
    %>
    <p>Đã lưu điểm thành công</p>
    <%
        }
    %>
</form>

<table style="margin-left: auto; margin-right: auto; width: 100%">
    <tr>
        <th>Mã sinh viên</th>
        <th>Tên sinh viên</th>
        <th>Ngành học</th>
        <th>Ngày sinh</th>
        <th>CC</th>
        <th>KT</th>
        <th>TN-TH</th>
        <th>Cuối kì</th>
        <th>ĐTB</th>
        <th>Điều kiện dự thi</th>
    </tr>
    <%
        //Lấy thông tin điểm hiện tại và điền vào bảng điểm cho từng sinh viên
        if(dsDiem == null) {
            for(int i = 0; i < dsSinhVien.size(); i++) {
                SinhVien sv = dsSinhVien.get(i);
                Diem diem = control.getDiemByMaSV_MaMon(sv.getMaSV(), maMon);
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                String ngaySinh = format.format(sv.getNgaySinh());
                String ketQua = diemController.xetDieuKienDuThi(diem);
    %>
    <tr>
        <td><%= sv.getMaSV() %></td>
        <td><%= sv.getTenSV() %></td>
        <td><%= sv.getNganhHoc() %></td>
        <td><%= ngaySinh %></td>
        <td><%= diem.getDiemCC() %></td>
        <td><%= diem.getDiemKT() %></td>
        <td><%= diem.getDiemTN_TH() %></td>
        <td><%= diem.getDiemCuoiKi() %></td>
        <td><%= diem.getDiemTrungBinh() %></td>
        <td><%= ketQua %></td>
    </tr>
    <%
        }
    }
    else {
        for(int i = 0; i < dsSinhVien.size(); i++) {
            SinhVien sv = dsSinhVien.get(i);
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            String ngaySinh = format.format(sv.getNgaySinh());
            Diem diem = dsDiem.get(i);
            String ketQua = diemController.xetDieuKienDuThi(diem);
    %>
    <tr>
        <td><%= sv.getMaSV() %></td>
        <td><%= sv.getTenSV() %></td>
        <td><%= sv.getNganhHoc() %></td>
        <td><%= ngaySinh %></td>
        <td><%= diem.getDiemCC() %></td>
        <td><%= diem.getDiemKT() %></td>
        <td><%= diem.getDiemTN_TH() %></td>
        <td><%= diem.getDiemCuoiKi() %></td>
        <td><%= diem.getDiemTrungBinh() %></td>
        <td><%= ketQua %></td>
    </tr>
    <%
            }
        }
        control.closeMySQLConnection();
    %>
</table>
</body>
</html>
