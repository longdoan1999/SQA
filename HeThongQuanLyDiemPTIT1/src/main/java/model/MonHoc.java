package model;

import model.Diem;
import java.util.Vector;

public class MonHoc {
	private String _maMon;
	private String _tenMon;
	public Vector<KiHocMonHoc> _kiHocMonHoc = new Vector<KiHocMonHoc>();
	public Diem _diem;

	public MonHoc() {
		throw new UnsupportedOperationException();
	}
        
        public MonHoc(String ID, String tenMon) {
            this._maMon = ID;
            this._tenMon = tenMon;
        }
	public String getMaMon() {
		return this._maMon;
	}

	public void setMaMon(String aMaMon) {
		this._maMon = aMaMon;
	}

	public String getTenMon() {
		return this._tenMon;
	}

	public void setTenMon(String aTenMon) {
		this._tenMon = aTenMon;
	}
}