package model;


public class Diem {
	private int _iD;
	private float _diemCC;
	private float _diemKT;
	private float _diemTN_TH;
	private float _diemCuoiKi;
	private float _diemTrungBinh;
	public String _sinhVien;
	public String _monHoc;

	public Diem() {
            _iD = -1; // Khởi tạo đối với bảng điểm chưa có thông tin
	}

        public Diem(int _iD, float _diemCC, float _diemKT, float _diemTN_TH, float _diemCuoiKi, float _diemTrungBinh) {
            this._iD = _iD;
            this._diemCC = _diemCC;
            this._diemKT = _diemKT;
            this._diemTN_TH = _diemTN_TH;
            this._diemCuoiKi = _diemCuoiKi;
            this._diemTrungBinh = _diemTrungBinh;
        }
        
	public int getID() {
		return this._iD;
	}

	public void setID(int aID) {
		this._iD = aID;
	}

	public float getDiemCC() {
		return this._diemCC;
	}

	public void setDiemCC(float aDiemCC) {
		this._diemCC = aDiemCC;
	}

	public float getDiemKT() {
		return this._diemKT;
	}

	public void setDiemKT(float aDiemKT) {
		this._diemKT = aDiemKT;
	}

	public float getDiemTN_TH() {
		return this._diemTN_TH;
	}

	public void setDiemTN_TH(float aDiemTN_TH) {
		this._diemTN_TH = aDiemTN_TH;
	}

	public float getDiemCuoiKi() {
		return this._diemCuoiKi;
	}

	public void setDiemCuoiKi(float aDiemCuoiKi) {
		this._diemCuoiKi = aDiemCuoiKi;
	}

	public float getDiemTrungBinh() {
		return this._diemTrungBinh;
	}

	public void setDiemTrungBinh(float aDiemTrungBinh) {
		this._diemTrungBinh = aDiemTrungBinh;
	}
}