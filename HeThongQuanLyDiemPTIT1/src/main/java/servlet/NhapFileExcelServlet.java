/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import controller.ExcelController;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import model.Diem;
import model.SinhVien;

/**
 *
 * @author Cao Sy Hai Long
 */
@WebServlet(name = "NhapFileExcelServlet", urlPatterns = {"/nhapFileExcelServlet"})
public class NhapFileExcelServlet extends HttpServlet {
    private static final long serialVersionUID = 102831973239L;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ParseException {
        Part filePart = request.getPart("excel");
        InputStream inputStream = filePart.getInputStream();
        String fileName = filePart.getSubmittedFileName();
        if(fileName.endsWith(".xls") || fileName.endsWith(".xlsx")) {
            // Lấy file excel upload về server
            String uploadFileDirectory = "\\Users\\thanhhieu\\Desktop\\test" + fileName;
            File fileToSave = new File(uploadFileDirectory);
            Files.copy(inputStream, fileToSave.toPath(), StandardCopyOption.REPLACE_EXISTING);
            request.setAttribute("isValid", Boolean.TRUE);
            ExcelController excelController = new ExcelController(uploadFileDirectory);
            excelController.readExcel();
            ArrayList<SinhVien> dsSinhVien = excelController.getDsSinhVien();
            ArrayList<Diem> dsDiem = excelController.getDsDiem();
            request.setAttribute("dsSinhVien", dsSinhVien);
            request.setAttribute("dsDiem", dsDiem);
        }
        else {
            request.setAttribute("isValid", Boolean.FALSE);
        }
        
        //Chuyển request và response sang GDdiem.jsp
        String url = "/GDdiem.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        System.out.println("Ma lop gui sang servlet: " + request.getParameter("maLop"));
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(NhapFileExcelServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, FileNotFoundException {
        try {
            processRequest(request, response);
        } catch (ParseException ex) {
            Logger.getLogger(NhapFileExcelServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
