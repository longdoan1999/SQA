-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlydiemptit
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kihocmonhoc`
--

DROP TABLE IF EXISTS `kihocmonhoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kihocmonhoc` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `MonHocMaMon` varchar(255) NOT NULL,
  `HocKiMaHocKy` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKKiHocMonHo203971` (`HocKiMaHocKy`),
  KEY `FKKiHocMonHo869226` (`MonHocMaMon`),
  CONSTRAINT `FKKiHocMonHo203971` FOREIGN KEY (`HocKiMaHocKy`) REFERENCES `hocki` (`MaHocKy`),
  CONSTRAINT `FKKiHocMonHo869226` FOREIGN KEY (`MonHocMaMon`) REFERENCES `monhoc` (`MaMon`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kihocmonhoc`
--

LOCK TABLES `kihocmonhoc` WRITE;
/*!40000 ALTER TABLE `kihocmonhoc` DISABLE KEYS */;
INSERT INTO `kihocmonhoc` VALUES (1,'csdl','HK20202'),(2,'ctdlgt','HK20202'),(3,'dbclpm','HK20202'),(4,'ltcpp','HK20202'),(5,'qlda','HK20202');
/*!40000 ALTER TABLE `kihocmonhoc` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-15 20:55:01
