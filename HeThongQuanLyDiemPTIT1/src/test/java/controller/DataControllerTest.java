package controller;

import model.*;
import org.junit.*;

import java.sql.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class DataControllerTest {

    @Test
    public void getLopByMaLopTest() throws SQLException {
        DataController DC = new DataController();
        Lop lop = DC.getLopByMaLop("L001");
        String test = "L001 1 Thứ hai 1 40";
        assertEquals(test,lop.getMaLop() + " "+ lop.getNhom() + " " + lop.getThu() + " " + lop.getKipHoc() + " " + lop.getSoLuong());
    }
    @Test
    public void getLopByMaLopNullTest() throws SQLException {
        DataController DC = new DataController();
        Lop lop = DC.getLopByMaLop("addwd");
        assertNull(lop);
    }
    @Test
    public void getMonHocByMaMonTest() throws SQLException{
        DataController DC = new DataController();
        MonHoc monHoc = DC.getMonHocByMaMon("csdl");
        String test = "csdl Cơ sở dữ liệu";
        assertEquals(test,monHoc.getMaMon() +" " + monHoc.getTenMon());
    }
    @Test
    public void getMonHocByMaMonNullTest() throws SQLException{
        DataController DC = new DataController();
        MonHoc monHoc = DC.getMonHocByMaMon("awdasd");
        assertNull(monHoc);
    }
    @Test
    public void getMonHocByMaGVTest() throws SQLException{
        DataController DC = new DataController();
        ArrayList<MonHoc> monHoc = DC.getMonHocByMaGV("GV001");
        String rs ="";
        for(int i=0;i!=monHoc.size();i++) {
            rs += monHoc.get(i).getTenMon()+"\n";
        }
        String test = "Cơ sở dữ liệu" + "\n" +"Cấu trúc dữ liệu & giải thuật" + "\n";
        assertEquals(test,rs);
    }
    @Test
    public void getMonHocByMaGVNullTest() throws SQLException{
        DataController DC = new DataController();
        ArrayList<MonHoc> monHoc = DC.getMonHocByMaGV("GV00");
        String rs = "";
        for(int i=0;i!=monHoc.size();i++) {
            rs += monHoc.get(i).getTenMon()+"\n";
        }
        String test = "";
        assertEquals(test,rs);
    }
    @Test
    public void getLopByMaMonTest() throws SQLException{
        DataController DC = new DataController();
        ArrayList<Lop> lop = DC.getLopByMaMon("csdl");
        String rs ="";
        for(int i=0;i!=lop.size();i++)
            rs += lop.get(i).getMaLop() + " "+ lop.get(i).getNhom() + " " + lop.get(i).getThu() + " " + lop.get(i).getKipHoc() + " " + lop.get(i).getSoLuong() + "\n";
        String test = "L001 1 Thứ hai 1 40\n" +
                "L002 2 Thứ năm 2 40\n" +
                "L003 3 Thứ sáu 3 40" +"\n";
        assertEquals(test,rs);
    }
    @Test
    public void getLopByMaMonNullTest() throws SQLException{
        DataController DC = new DataController();
        ArrayList<Lop> lop = DC.getLopByMaMon("csd");
        String rs ="";
        for(int i=0;i!=lop.size();i++)
            rs += lop.get(i).getMaLop() + " "+ lop.get(i).getNhom() + " " + lop.get(i).getThu() + " " + lop.get(i).getKipHoc() + " " + lop.get(i).getSoLuong() + "\n";
        String test = "";
        assertEquals(test,rs);
    }
    @Test
    public void getSinhVienByMaLop() throws SQLException{
        DataController DC = new DataController();
        ArrayList<SinhVien> sv = DC.getSinhVienByMaLop("L001");
        String rs = "";
        for(int i=0;i!=sv.size();i++){
            rs += sv.get(i).getMaSV() + " " + sv.get(i).getTenSV() + " " + sv.get(i).getNgaySinh() + " " + sv.get(i).getNganhHoc() +"\n";
        }
        String test =
                "B17DCCN001 Phạm Hải Nam 1999-06-17 Công nghệ thông tin\n" +
                "B17DCCN002 Nguyễn Đăng Phước 1999-12-26 Công nghệ thông tin\n" +
                "B17DCCN003 Trương Văn Thọ 1999-05-22 Công nghệ thông tin\n" +
                "B17DCCN004 Nguyễn Văn Đông 1999-08-04 Công nghệ thông tin\n" +
                "B17DCCN005 Nguyễn Văn Chiên 1999-08-24 Công nghệ thông tin\n" +
                "B17DCCN006 Hoàng Bá Ý 1999-10-01 Công nghệ thông tin\n" +
                "B17DCCN007 Tiêu Khánh Duy 1999-09-01 Công nghệ thông tin\n" +
                "B17DCCN008 Lê Thành Duy 1999-07-07 Công nghệ thông tin\n" +
                "B17DCCN009 Nguyễn Hữu Phúc 1999-01-21 Công nghệ thông tin\n" +
                "B17DCCN010 Đỗ Thu Trang 1999-04-21 Công nghệ thông tin\n" +
                "B17DCCN011 Trần Duy Chiến 1999-02-27 Công nghệ thông tin\n" +
                "B17DCCN012 Đặng Đình Hiển 1999-03-11 Công nghệ thông tin\n" +
                "B17DCCN013 Nguyễn Minh Quân 1999-03-12 Công nghệ thông tin\n" +
                "B17DCCN014 Nguyễn Hữu Thọ 1999-03-30 Công nghệ thông tin\n" +
                "B17DCCN015 Tạ Minh Quang 1999-12-17 Công nghệ thông tin\n" +
                "B17DCCN016 Đỗ Thị Thanh Hoa 1999-09-09 Công nghệ thông tin\n" +
                "B17DCCN017 Mai Đức Phương 1999-07-31 Công nghệ thông tin\n" +
                "B17DCCN018 Đỗ Văn Hùng 1999-05-24 Công nghệ thông tin\n" +
                "B17DCCN019 Lê Minh Đức 1999-06-26 Công nghệ thông tin\n" +
                "B17DCCN020 Lê Hải Dương 1999-04-22 Công nghệ thông tin\n" +
                "B17DCCN021 Nguyễn Văn Đạt 1999-03-05 Công nghệ thông tin\n" +
                "B17DCCN022 Nguyễn Đình Long 1999-10-03 Công nghệ thông tin\n" +
                "B17DCCN023 Phạm Minh Đạt 1999-04-28 Công nghệ thông tin\n" +
                "B17DCCN024 Nguyễn Phương Nam 1999-01-28 Công nghệ thông tin\n" +
                "B17DCCN025 Nguyễn Đỗ Tuấn Minh 1999-03-30 Công nghệ thông tin\n" +
                "B17DCCN026 Đỗ Tú Anh 1999-11-15 Công nghệ thông tin\n" +
                "B17DCCN027 Phan Đức Duẩn 1999-05-16 Công nghệ thông tin\n" +
                "B17DCCN028 Đỗ Tiến Đạt 1999-12-24 Công nghệ thông tin\n" +
                "B17DCCN029 Ngô Thị Giang 1999-04-01 Công nghệ thông tin\n" +
                "B17DCCN030 Hoàng Việt Hàn 1999-06-18 Công nghệ thông tin\n" +
                "B17DCCN031 Đinh Quang Linh 1999-03-30 Công nghệ thông tin\n" +
                "B17DCCN032 Nguyễn Đình Linh 1999-12-08 Công nghệ thông tin\n" +
                "B17DCCN033 Nguyễn Quốc Thái 1999-05-23 Công nghệ thông tin\n" +
                "B17DCCN034 Nguyễn Đức Hưng 1999-04-17 Công nghệ thông tin\n" +
                "B17DCCN035 Đinh Sơn Hà 1999-09-08 Công nghệ thông tin\n" +
                "B17DCCN036 Nguyễn Thị Ngọc Bích 1999-11-03 Công nghệ thông tin\n" +
                "B17DCCN037 Hoàng Đình Nam 1999-01-12 Công nghệ thông tin\n" +
                "B17DCCN038 Nguyễn Phương Nam 1999-05-23 Công nghệ thông tin\n" +
                "B17DCCN039 Trần Thái Sơn 1999-12-12 Công nghệ thông tin\n" +
                "B17DCCN040 Nguyễn Như Tuấn 1999-07-28 Công nghệ thông tin\n";
        assertEquals(test,rs);
    }
    @Test
    public void getSinhVienByMaLopNull() throws SQLException{
        DataController DC = new DataController();
        ArrayList<SinhVien> sv = DC.getSinhVienByMaLop("L00");
        String rs = "";
        for(int i=0;i!=sv.size();i++){
            rs += sv.get(i).getMaSV() + " " + sv.get(i).getTenSV() + " " + sv.get(i).getNgaySinh() + " " + sv.get(i).getNganhHoc() +"\n";
        }
        String test = "";
        assertEquals(test,rs);
    }
    @Test
    public void getDiemByMaSV_MaMonTest() throws SQLException{
        DataController DC = new DataController();
        Diem diem = DC.getDiemByMaSV_MaMon("B17DCCN001","csdl");
        String rs =  diem.getDiemCC() + " " + diem.getDiemKT() + " " + diem.getDiemTN_TH() + " " + diem.getDiemCuoiKi() + " " + diem.getDiemTrungBinh();
        String test = "5.0 5.0 5.0 5.0 5.0";
        assertEquals(test,rs);
    }
    @Test
    public void getDiemByMaSVNull_MaMonNullTest() throws SQLException{
        DataController DC = new DataController();
        Diem diem = DC.getDiemByMaSV_MaMon("B17DCCN00","csd");
        String rs =  diem.getDiemCC() + " " + diem.getDiemKT() + " " + diem.getDiemTN_TH() + " " + diem.getDiemCuoiKi() + " " + diem.getDiemTrungBinh();
        assertNull(rs);
    }
    @Test
    public void getDiemByMaSVNull_MaMonTest() throws SQLException{
        DataController DC = new DataController();
        Diem diem = DC.getDiemByMaSV_MaMon("B17DCCN00","csdl");
        String rs = diem.getDiemCC() + " " + diem.getDiemKT() + " " + diem.getDiemTN_TH() + " " + diem.getDiemCuoiKi() + " " + diem.getDiemTrungBinh();
        assertNull(rs);
    }
    @Test
    public void getDiemByMaSV_MaMonNullTest() throws SQLException{
        DataController DC = new DataController();
        Diem diem = DC.getDiemByMaSV_MaMon("B17DCCN00","csd");
        String rs = diem.getDiemCC() + " " + diem.getDiemKT() + " " + diem.getDiemTN_TH() + " " + diem.getDiemCuoiKi() + " " + diem.getDiemTrungBinh();
        assertNull(rs);
    }
    @Test
    public void addDiemTest() throws SQLException{
        DataController DC = new DataController();
        Diem diem = new Diem(2,5,5,5,5, 5);
        diem._monHoc = "csdl";
        diem._sinhVien = "B17DCCN002";
        DC.addDiem(diem);

        Diem diemTest = DC.getDiemByMaSV_MaMon("B17DCCN002","csdl");
        String rs = "B17DCCN002" + " " + "csdl" + " " + diemTest.getDiemCC() + " " + diemTest.getDiemKT() + " " + diemTest.getDiemTN_TH() + " " + diemTest.getDiemCuoiKi() + " " + diemTest.getDiemTrungBinh();
        String test = "B17DCCN002 csdl 5.0 5.0 5.0 5.0 5.0";
        assertEquals(test,rs);
    }
    @Test
    public void updateDiemTest() throws SQLException{

        DataController DC = new DataController();
        Diem diem = new Diem(1,5,5,0,5, 4);
        diem._monHoc = "csdl";
        diem._sinhVien = "B17DCCN001";
        DC.updateDiem(diem);

        Diem diemTest = DC.getDiemByMaSV_MaMon("B17DCCN001","csdl");
        String rs = "B17DCCN001" + " " + "csdl" + " " + diemTest.getDiemCC() + " " + diemTest.getDiemKT() + " " + diemTest.getDiemTN_TH() + " " + diemTest.getDiemCuoiKi() + " " + diemTest.getDiemTrungBinh();
        String test = "B17DCCN001 csdl 5.0 5.0 0.0 5.0 4.0";
        assertEquals(test,rs);
    }
}