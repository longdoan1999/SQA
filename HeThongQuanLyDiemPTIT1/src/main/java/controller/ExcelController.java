/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import model.Diem;
import model.SinhVien;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



/**
 *
 * @author Cao Sy Hai Long
 */
public class ExcelController {
    private static final int COT_TEN_SINH_VIEN = 1;
    private static final int COT_MA_SINH_VIEN = 0;
    private static final int COT_NGANH_HOC = 2;
    private static final int COT_NGAY_SINH = 3;
    private static final int COT_DIEM_CC = 4;
    private static final int COT_DIEM_KT = 5;
    private static final int COT_DIEM_TN_TH = 6;
    private static final int COT_DIEM_CUOI_KY = 7;
    private String filePath;
    private Workbook workbook;
    private Sheet sheet;
    private Row row;
    private Cell cell;
    private ArrayList<SinhVien> dsSinhVien;
    private ArrayList<Diem> dsDiem;
    
    public ExcelController(String excelPath) {
        this.filePath = excelPath;
        dsSinhVien = new ArrayList<>();
        dsDiem = new ArrayList<>();
    }

    public ArrayList<SinhVien> getDsSinhVien() {
        return dsSinhVien;
    }

    public ArrayList<Diem> getDsDiem() {
        return dsDiem;
    }
    
    // Lấy danh sách điểm và thông tin của sinh viên từ file Excel được nhập từ giao diện điểm
    public void readExcel() throws FileNotFoundException, IOException, ParseException {
        InputStream in = new FileInputStream(new File(filePath));
        if(getWorkbook(in, filePath)) {
            // Lấy sheet nếu là file excel
            sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            // Duyệt hàng trong bảng từ hàng đầu đến hàng cuối
            while(rowIterator.hasNext()) {
                Row row = rowIterator.next();
                if(row.getRowNum() == 0) {
                    // Bỏ qua hàng header
                    continue;
                }
                Iterator<Cell> cellIterator = row.cellIterator();
                SinhVien sv = new SinhVien();
                Diem diem = new Diem();
                while(cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    Object cellValue = getCellValue(cell);
                    if(cellValue == null || cellValue.toString().isEmpty())
                        continue;
                    
                    int columnIndex = cell.getColumnIndex();
                    switch(columnIndex) {
                        case COT_MA_SINH_VIEN:
                            sv.setMaSV((String)cellValue);
                            break;
                        case COT_TEN_SINH_VIEN:
                            sv.setTenSV((String)cellValue);
                            break;
                        case COT_NGANH_HOC:
                            sv.setNganhHoc((String)cellValue);
                            break;
                        case COT_NGAY_SINH:
                            if (cellValue instanceof Date)
                                sv.setNgaySinh((Date)cellValue);
                            else
                                sv.setNgaySinh(new SimpleDateFormat("dd/MM/yyyy").parse((String)cellValue));
                            break;
                        case COT_DIEM_CC:
                            diem.setDiemCC(((Double)cellValue).intValue());
                            break;
                        case COT_DIEM_KT:
                            diem.setDiemKT(((Double)cellValue).floatValue());
                            break;
                        case COT_DIEM_TN_TH:
                            diem.setDiemTN_TH(((Double)cellValue).floatValue());
                            break;
                        case COT_DIEM_CUOI_KY:
                            diem.setDiemCuoiKi(((Double)cellValue).floatValue());
                            break;
                    }
                }
                dsSinhVien.add(sv);
                dsDiem.add(diem);
            }
        }
        
    }
    
    public boolean getWorkbook(InputStream in, String excelFilePath) throws IOException {
        if(excelFilePath.endsWith("xlsx")) {
            this.workbook = new XSSFWorkbook(in);
            return true;
        }
        else if(excelFilePath.endsWith("xls")) {
            this.workbook = new HSSFWorkbook(in);
            return true;
        } else
            return false;
    }
    
    public Object getCellValue(Cell cell) throws ParseException {
        CellType cellType = cell.getCellTypeEnum();
        Object cellValue = null;
        switch(cellType) {
            case STRING:
                cellValue = cell.getStringCellValue();
                break;
            case NUMERIC:
                if(DateUtil.isCellDateFormatted(cell)) {
                    cellValue = DateUtil.getJavaDate(cell.getNumericCellValue());
                }
                else
                cellValue = cell.getNumericCellValue();
                System.out.println(cellValue);
                break;
        }
        return cellValue;
    }

    public Workbook getWorkbook() {
        return workbook;
    }
}
