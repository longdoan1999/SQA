package model;

import java.util.Date;
import java.util.Vector;

public class GiangVien {
	private String _maGV;
	private String _tenGV;
	private String _khoa;
	private Date _ngaySinh;
	public TaiKhoan _taiKhoan;
	public Vector<Lop> _lop = new Vector<Lop>();
	public ThongTinLienHe _thongTinLienHe;

	public GiangVien() {
		throw new UnsupportedOperationException();
	}

	public String getMaGV() {
		return this._maGV;
	}

	public void setMaGV(String aMaGV) {
		this._maGV = aMaGV;
	}

	public String getTenGV() {
		return this._tenGV;
	}

	public void setTenGV(String aTenGV) {
		this._tenGV = aTenGV;
	}

	public String getKhoa() {
		return this._khoa;
	}

	public void setKhoa(String aKhoa) {
		this._khoa = aKhoa;
	}

	public Date getNgaySinh() {
		return this._ngaySinh;
	}

	public void setNgaySinh(Date aNgaySinh) {
		this._ngaySinh = aNgaySinh;
	}
}