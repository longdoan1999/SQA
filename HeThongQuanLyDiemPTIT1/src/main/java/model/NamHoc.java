package model;

import java.util.Vector;

public class NamHoc {
	private int _iD;
	private String _namHoc;
	public Vector<HocKi> _hocKi = new Vector<HocKi>();

	public String getNamHoc() {
		return this._namHoc;
	}

	public void setNamHoc(String aNamHoc) {
		this._namHoc = aNamHoc;
	}

	public NamHoc() {
		throw new UnsupportedOperationException();
	}
}