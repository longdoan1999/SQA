-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlydiemptit
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sinhvien`
--

DROP TABLE IF EXISTS `sinhvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sinhvien` (
  `MaSV` varchar(255) NOT NULL,
  `TenSV` varchar(255) DEFAULT NULL,
  `NganhHoc` varchar(255) DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  PRIMARY KEY (`MaSV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sinhvien`
--

LOCK TABLES `sinhvien` WRITE;
/*!40000 ALTER TABLE `sinhvien` DISABLE KEYS */;
INSERT INTO `sinhvien` VALUES ('B17DCCN001','Phạm Hải Nam','Công nghệ thông tin','1999-06-17'),('B17DCCN002','Nguyễn Đăng Phước','Công nghệ thông tin','1999-12-26'),('B17DCCN003','Trương Văn Thọ','Công nghệ thông tin','1999-05-22'),('B17DCCN004','Nguyễn Văn Đông','Công nghệ thông tin','1999-08-04'),('B17DCCN005','Nguyễn Văn Chiên','Công nghệ thông tin','1999-08-24'),('B17DCCN006','Hoàng Bá Ý','Công nghệ thông tin','1999-10-01'),('B17DCCN007','Tiêu Khánh Duy','Công nghệ thông tin','1999-09-01'),('B17DCCN008','Lê Thành Duy','Công nghệ thông tin','1999-07-07'),('B17DCCN009','Nguyễn Hữu Phúc','Công nghệ thông tin','1999-01-21'),('B17DCCN010','Đỗ Thu Trang','Công nghệ thông tin','1999-04-21'),('B17DCCN011','Trần Duy Chiến','Công nghệ thông tin','1999-02-27'),('B17DCCN012','Đặng Đình Hiển','Công nghệ thông tin','1999-03-11'),('B17DCCN013','Nguyễn Minh Quân','Công nghệ thông tin','1999-03-12'),('B17DCCN014','Nguyễn Hữu Thọ','Công nghệ thông tin','1999-03-30'),('B17DCCN015','Tạ Minh Quang','Công nghệ thông tin','1999-12-17'),('B17DCCN016','Đỗ Thị Thanh Hoa','Công nghệ thông tin','1999-09-09'),('B17DCCN017','Mai Đức Phương','Công nghệ thông tin','1999-07-31'),('B17DCCN018','Đỗ Văn Hùng','Công nghệ thông tin','1999-05-24'),('B17DCCN019','Lê Minh Đức','Công nghệ thông tin','1999-06-26'),('B17DCCN020','Lê Hải Dương','Công nghệ thông tin','1999-04-22'),('B17DCCN021','Nguyễn Văn Đạt','Công nghệ thông tin','1999-03-05'),('B17DCCN022','Nguyễn Đình Long','Công nghệ thông tin','1999-10-03'),('B17DCCN023','Phạm Minh Đạt','Công nghệ thông tin','1999-04-28'),('B17DCCN024','Nguyễn Phương Nam','Công nghệ thông tin','1999-01-28'),('B17DCCN025','Nguyễn Đỗ Tuấn Minh','Công nghệ thông tin','1999-03-30'),('B17DCCN026','Đỗ Tú Anh','Công nghệ thông tin','1999-11-15'),('B17DCCN027','Phan Đức Duẩn','Công nghệ thông tin','1999-05-16'),('B17DCCN028','Đỗ Tiến Đạt','Công nghệ thông tin','1999-12-24'),('B17DCCN029','Ngô Thị Giang','Công nghệ thông tin','1999-04-01'),('B17DCCN030','Hoàng Việt Hàn','Công nghệ thông tin','1999-06-18'),('B17DCCN031','Đinh Quang Linh','Công nghệ thông tin','1999-03-30'),('B17DCCN032','Nguyễn Đình Linh','Công nghệ thông tin','1999-12-08'),('B17DCCN033','Nguyễn Quốc Thái','Công nghệ thông tin','1999-05-23'),('B17DCCN034','Nguyễn Đức Hưng','Công nghệ thông tin','1999-04-17'),('B17DCCN035','Đinh Sơn Hà','Công nghệ thông tin','1999-09-08'),('B17DCCN036','Nguyễn Thị Ngọc Bích','Công nghệ thông tin','1999-11-03'),('B17DCCN037','Hoàng Đình Nam','Công nghệ thông tin','1999-01-12'),('B17DCCN038','Nguyễn Phương Nam','Công nghệ thông tin','1999-05-23'),('B17DCCN039','Trần Thái Sơn','Công nghệ thông tin','1999-12-12'),('B17DCCN040','Nguyễn Như Tuấn','Công nghệ thông tin','1999-07-28');
/*!40000 ALTER TABLE `sinhvien` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-15 20:55:02
