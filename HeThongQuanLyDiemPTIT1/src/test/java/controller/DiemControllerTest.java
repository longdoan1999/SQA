package controller;

import model.*;
import org.junit.*;

import static org.junit.jupiter.api.Assertions.*;

public class DiemControllerTest {
    @Test
    public void tinhDiemTrungBinhTest(){
        DiemController DiC = new DiemController();
        Diem diem = new Diem(1,9.0f,7.0f,3.0f,3.0f,0.0f);
        assertEquals(4.0f,DiC.tinhDiemTrungBinh(0.1f,0.1f,0.2f,0.6f,diem));
    }
    @Test
    public void xetDieuKienDuThi_DuocThi_Test(){
    Diem diem = new Diem(1,9,6.5f,3,3,4);
    DiemController DiC = new DiemController();
    assertEquals("Được thi", DiC.xetDieuKienDuThi(diem));
    }
    @Test
    public void xetDieuKienDuThi_KhongDuocThi_TB39_Test(){
        Diem diem = new Diem(1,9,6,3,3,3.9f);
        DiemController DiC = new DiemController();
        assertEquals("Không được thi", DiC.xetDieuKienDuThi(diem));
    }
    @Test
    public void xetDieuKienDuThi_KhongDuocThi_CC0_Test(){
        Diem diem = new Diem(1,0,10,10,10,9);
        DiemController DiC = new DiemController();
        assertEquals("Không được thi", DiC.xetDieuKienDuThi(diem));
    }
    @Test
    public void xetDieuKienDuThi_KhongDuocThi_KT0_Test(){
        Diem diem = new Diem(1,10,0,10,10,9);
        DiemController DiC = new DiemController();
        assertEquals("Không được thi", DiC.xetDieuKienDuThi(diem));
    }
    @Test
    public void xetDieuKienDuThi_KhongDuocThi_TNTH0_Test(){
        Diem diem = new Diem(1,10,10,0,10,9);
        DiemController DiC = new DiemController();
        assertEquals("Không được thi", DiC.xetDieuKienDuThi(diem));
    }
    @Test
    public void xetDieuKienDuThi_KhongDuocThi_CuoiKi0_Test(){
        Diem diem = new Diem(1,10,10,10,0,4);
        DiemController DiC = new DiemController();
        assertEquals("Không được thi", DiC.xetDieuKienDuThi(diem));
    }
    @Test
    public void xetDieuKienDuThi_KhongDuocThi_Diem0_Test(){
        Diem diem = new Diem(1,0,0,0,0,0);
        DiemController DiC = new DiemController();
        assertEquals("Không được thi", DiC.xetDieuKienDuThi(diem));
    }
}