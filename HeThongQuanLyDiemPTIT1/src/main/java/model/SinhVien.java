package model;

import java.util.Date;
import java.util.Vector;

public class SinhVien {
	private String _maSV;
	private String _tenSV;
	private String _nganhHoc;
	private Date _ngaySinh;
	public Vector<Lop> _lop = new Vector<Lop>();
	public Vector<Diem> _diem = new Vector<Diem>();
	public ThongTinLienHe _thongTinLienHe;

	public SinhVien() {
	}

        public SinhVien(String _maSV, String _tenSV, String _nganhHoc, Date _ngaySinh) {
            this._maSV = _maSV;
            this._tenSV = _tenSV;
            this._nganhHoc = _nganhHoc;
            this._ngaySinh = _ngaySinh;
        }

	public String getMaSV() {
		return this._maSV;
	}

	public void setMaSV(String aMaSV) {
		this._maSV = aMaSV;
	}

	public String getTenSV() {
		return this._tenSV;
	}

	public void setTenSV(String aTenSV) {
		this._tenSV = aTenSV;
	}

	public String getNganhHoc() {
		return this._nganhHoc;
	}

	public void setNganhHoc(String aNganhHoc) {
		this._nganhHoc = aNganhHoc;
	}

	public Date getNgaySinh() {
		return this._ngaySinh;
	}

	public void setNgaySinh(Date aNgaySinh) {
		this._ngaySinh = aNgaySinh;
	}
}