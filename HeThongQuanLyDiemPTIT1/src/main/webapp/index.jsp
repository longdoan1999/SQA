<%-- 
    Document   : index
    Created on : Apr 14, 2021, 1:58:21 AM
    Author     : Cao Sy Hai Long
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Đăng nhập</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .center{
                margin-left: auto;
                margin-right: auto;
            }
            .button_center {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div>
            <h1 style = "text-align: center">Đăng nhập</h1>
        </div>
        <%
            
        %>
        <div>
            <form action="verifyLogin.jsp" method="post">
                <table class="center">
                    <tr>
                        <td style="text-align: end">
                            <label for="tenTK">Tên tài khoản:</label>
                        </td>
                        <td>
                            <input type="text" id="tenTK" name="tenTK"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: end">
                            <label for="matKhau">Mật khẩu:</label>
                        </td>
                        <td>
                            <input type="password" name="matKhau" id="matKhau"/>
                        </td>
                    </tr>
                </table>
                <div class="button_center">
                <button style="align-content: center" type="submit">Đăng nhập</button>
                </div>
            </form>
        </div>
    </body>
</html>
