package controller;

import model.*;
import org.apache.poi.ss.usermodel.*;
import org.junit.*;

import java.io.*;
import java.nio.file.*;
import java.text.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class ExcelControllerTest {

    @Test
    public void readExcelTest() throws IOException, ParseException {
        ExcelController ec = new ExcelController("/Users/thanhhieu/Desktop/test" + "/testFile2.xlsx");
        ec.readExcel();
        ArrayList<SinhVien> dsSinhvien = ec.getDsSinhVien();
        ArrayList<Diem> dsDiem = ec.getDsDiem();
        SinhVien s = new SinhVien("B17DCCN001", "Phạm Hải Nam", "Công nghệ thông tin",
                new SimpleDateFormat("dd/MM/yyyy").parse("17/06/1999"));
        Diem d = new Diem(0, 10, 10, 10, 5, 0);
        assertEquals(s.getMaSV(), dsSinhvien.get(0).getMaSV());
        assertEquals(s.getTenSV(), dsSinhvien.get(0).getTenSV());
        assertEquals(s.getNganhHoc(), dsSinhvien.get(0).getNganhHoc());
        assertEquals(s.getNgaySinh(), dsSinhvien.get(0).getNgaySinh());
        assertEquals(d.getDiemCC(), dsDiem.get(0).getDiemCC());
        assertEquals(d.getDiemKT(), dsDiem.get(0).getDiemKT());
        assertEquals(d.getDiemTN_TH(), dsDiem.get(0).getDiemTN_TH());
        assertEquals(d.getDiemCuoiKi(), dsDiem.get(0).getDiemCuoiKi());
    }
    @Test
    public void getWorkBook_xlsx_Test() throws IOException {
        String filePath = "/Users/thanhhieu/Desktop/test" + "/testFile2.xlsx";
        ExcelController EC = new ExcelController(filePath);
        InputStream in = new FileInputStream(filePath);
        boolean test = EC.getWorkbook(in,filePath);
        assertTrue(test);
    }
    @Test
    public void getWorkBook_xls_Test() throws IOException {
        String filePath = "/Users/thanhhieu/Desktop/test" + "/testFile3.xls";
        ExcelController EC = new ExcelController(filePath);
        InputStream in = new FileInputStream(filePath);
        boolean test = EC.getWorkbook(in,filePath);
        assertTrue(test);
    }
    @Test
    public void getWorkBook_nonExcel_Test() throws IOException {
        String filePath = "/Users/thanhhieu/Desktop/test" + "/testFile4.docx";
        ExcelController EC = new ExcelController(filePath);
        InputStream in = new FileInputStream(filePath);
        boolean test = EC.getWorkbook(in,filePath);
        assertFalse(test);
    }
    @Test
    public void getCellValue_STRING_test() throws IOException, ParseException {
        String filePath = "/Users/thanhhieu/Desktop/test" + "/testFile3.xls";
        ExcelController EC = new ExcelController(filePath);
        InputStream in = new FileInputStream(filePath);
        EC.getWorkbook(in,filePath);
        Sheet sheet = EC.getWorkbook().getSheetAt(0);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(0);
        assertEquals("String",EC.getCellValue(cell));
    }
    @Test
    public void getCellValue_NUMERIC_test() throws IOException, ParseException {
        String filePath = "/Users/thanhhieu/Desktop/test" + "/testFile3.xls";
        ExcelController EC = new ExcelController(filePath);
        InputStream in = new FileInputStream(filePath);
        EC.getWorkbook(in,filePath);
        Sheet sheet = EC.getWorkbook().getSheetAt(0);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(1);
        assertEquals(1.0,EC.getCellValue(cell));
    }
    @Test
    public void getCellValue_NUMERIC_DATE_test() throws IOException, ParseException {
        String filePath = "/Users/thanhhieu/Desktop/test" + "/testFile3.xls";
        ExcelController EC = new ExcelController(filePath);
        InputStream in = new FileInputStream(filePath);
        EC.getWorkbook(in,filePath);
        Sheet sheet = EC.getWorkbook().getSheetAt(0);
        Row row = sheet.getRow(0);
        Cell cell = row.getCell(2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = dateFormat.parse("14/02/1999");
        assertEquals(date,EC.getCellValue(cell));
    }
}