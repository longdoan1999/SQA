package model;

import java.util.Vector;

public class Lop {
	private String _maLop;
	private int _nhom;
	private int _soLuong;
	private int _kipHoc;
	private String _thu;
	public Vector<SinhVien> _sinhVien = new Vector<SinhVien>();
	public GiangVien _giangVien;
	public KiHocMonHoc _kiHocMonHoc;

	public Lop() {
		throw new UnsupportedOperationException();
	}

    public Lop(String _maLop, int _nhom, int _soLuong, int _kipHoc, String _thu) {
        this._maLop = _maLop;
        this._nhom = _nhom;
        this._soLuong = _soLuong;
        this._kipHoc = _kipHoc;
        this._thu = _thu;
    }
        

	public String getMaLop() {
		return this._maLop;
	}

	public void setMaLop(String aMaLop) {
		this._maLop = aMaLop;
	}

	public int getNhom() {
		return this._nhom;
	}

	public void setNhom(int aNhom) {
		this._nhom = aNhom;
	}

	public int getSoLuong() {
		return this._soLuong;
	}

	public void setSoLuong(int aSoLuong) {
		this._soLuong = aSoLuong;
	}

	public int getKipHoc() {
		return this._kipHoc;
	}

	public void setKipHoc(int aKipHoc) {
		this._kipHoc = aKipHoc;
	}

	public String getThu() {
		return this._thu;
	}

	public void setThu(String aThu) {
		this._thu = aThu;
	}
}