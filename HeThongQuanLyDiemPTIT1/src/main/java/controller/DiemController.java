/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.Diem;

/**
 *
 * @author Cao Sy Hai Long <your.name at your.org>
 */
public class DiemController {
    public float tinhDiemTrungBinh(float cc, float kt, float tn_th, float cuoiKi, Diem diem) {
        float diemCC = diem.getDiemCC();
        float diemKT = diem.getDiemKT();
        float diemTN_TH = diem.getDiemTN_TH();
        float diemCuoiKi = diem.getDiemCuoiKi();
        float diemTrungBinh = (diemCC * cc) + (diemKT * kt) + (diemTN_TH * tn_th) + (diemCuoiKi * cuoiKi);
        return diemTrungBinh;
    }

    public String xetDieuKienDuThi(Diem diem) {
        if(diem.getDiemTrungBinh() >= 4 && diem.getDiemCC() >0 && diem.getDiemKT() > 0 && diem.getDiemCuoiKi() > 0 && diem.getDiemTN_TH() > 0)
            return"Được thi";
        else
            return "Không được thi";
    }
}
