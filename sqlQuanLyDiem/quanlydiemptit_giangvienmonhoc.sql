-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlydiemptit
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `giangvienmonhoc`
--

DROP TABLE IF EXISTS `giangvienmonhoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `giangvienmonhoc` (
  `ID` int NOT NULL,
  `GiangVienMaGV` varchar(255) NOT NULL,
  `MonHocMaMon` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `GiangVienMaGV` (`GiangVienMaGV`),
  KEY `MonHocMaMon` (`MonHocMaMon`),
  CONSTRAINT `giangvienmonhoc_ibfk_1` FOREIGN KEY (`GiangVienMaGV`) REFERENCES `giangvien` (`MaGV`),
  CONSTRAINT `giangvienmonhoc_ibfk_2` FOREIGN KEY (`MonHocMaMon`) REFERENCES `monhoc` (`MaMon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `giangvienmonhoc`
--

LOCK TABLES `giangvienmonhoc` WRITE;
/*!40000 ALTER TABLE `giangvienmonhoc` DISABLE KEYS */;
INSERT INTO `giangvienmonhoc` VALUES (1,'GV001','csdl'),(2,'GV001','ctdlgt'),(3,'GV002','ltcpp'),(4,'GV003','dbclpm'),(5,'GV003','qlda');
/*!40000 ALTER TABLE `giangvienmonhoc` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-15 20:55:01
