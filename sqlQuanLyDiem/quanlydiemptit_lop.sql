-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: quanlydiemptit
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `lop`
--

DROP TABLE IF EXISTS `lop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lop` (
  `MaLop` varchar(255) NOT NULL,
  `KiHocMonHocID` int NOT NULL,
  `GiangVienMaGV` varchar(255) NOT NULL,
  `Nhom` int NOT NULL,
  `SoLuong` int NOT NULL,
  `KipHoc` int NOT NULL,
  `Thu` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`MaLop`),
  KEY `FKLop728728` (`GiangVienMaGV`),
  KEY `FKLop201941` (`KiHocMonHocID`),
  CONSTRAINT `FKLop201941` FOREIGN KEY (`KiHocMonHocID`) REFERENCES `kihocmonhoc` (`ID`),
  CONSTRAINT `FKLop728728` FOREIGN KEY (`GiangVienMaGV`) REFERENCES `giangvien` (`MaGV`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lop`
--

LOCK TABLES `lop` WRITE;
/*!40000 ALTER TABLE `lop` DISABLE KEYS */;
INSERT INTO `lop` VALUES ('L001',1,'GV001',1,40,1,'Thứ hai'),('L002',1,'GV001',2,40,2,'Thứ năm'),('L003',1,'GV001',3,40,3,'Thứ sáu'),('L004',2,'GV001',1,40,4,'Thứ ba'),('L005',2,'GV001',2,40,5,'Thứ tư'),('L006',2,'GV001',3,40,6,'Thứ năm'),('L007',3,'GV003',1,40,1,'Thứ sáu'),('L008',3,'GV003',2,40,2,'Thứ hai'),('L009',3,'GV003',3,40,3,'Thứ tư'),('L010',4,'GV002',1,40,1,'Thứ hai'),('L011',4,'GV002',2,40,2,'Thứ năm'),('L012',4,'GV002',3,40,3,'Thứ sáu'),('L013',5,'GV003',1,40,4,'Thứ tư'),('L014',5,'GV003',2,40,5,'Thứ hai'),('L015',5,'GV003',3,40,6,'Thứ sáu');
/*!40000 ALTER TABLE `lop` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-15 20:55:01
