<%-- 
    Document   : home
    Created on : Apr 13, 2021, 10:01:15 PM
    Author     : Cao Sy Hai Long
--%>


<%@page import="controller.LoginController"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        .center{
            margin-left: auto;
            margin-right: auto;
        }
        .button_center {
            text-align: center;
        }
    </style>
    <title>Đăng nhập</title>
</head>
<body>
<div>
    <h1 style = "text-align: center">Đăng nhập</h1>
</div>
<div>
    <%
        String loginName = request.getParameter("tenTK");
        String loginPassword = request.getParameter("matKhau");
        System.out.println(loginName + " " + loginPassword);
        LoginController control = new LoginController();
        String ID = "";
        if(control.checkEmpty(loginName,loginPassword)==false){
    %>
    <p style="color: red; text-align: center">Tên tài khoản hoặc mật khẩu không được để trống</p>
    <%
    }
    else if(control.verify(loginName, loginPassword, ID)==false) {
    %>
    <p style="color: red; text-align: center">Tên tài khoản hoặc mật khẩu không đúng</p>
    <%
        }
        else {
            String url = "/GDchonMonHoc.jsp";
            RequestDispatcher rd = (RequestDispatcher) request.getServletContext().getRequestDispatcher(url);
            String maGV = control.getMaGV(ID);
            System.out.println("MaGV: " + maGV);
            request.setAttribute("ID", maGV);
            rd.forward(request, response);
        }
    %>

    <form action="verifyLogin.jsp" method="post">
        <table class="center">
            <tr>
                <td style="text-align: end">
                    <label for="tenTK">Tên tài khoản:</label>
                </td>
                <td>
                    <input type="text" id="tenTK" name="tenTK" />
                </td>
            </tr>
            <tr>
                <td style="text-align: end">
                    <label for="matKhau">Mật khẩu:</label>
                </td>
                <td>
                    <input type="password" id="matKhau" name="matKhau" />
                </td>
            </tr>
        </table>
        <div class="button_center">
            <button style="align-content: center" type="submit" onsubmit="">Đăng nhập</button>
        </div>
    </form>
</div>
</body>
</html>
