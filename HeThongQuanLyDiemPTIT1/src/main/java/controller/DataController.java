/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.*;

import model.Diem;
import model.GiangVien;
import model.Lop;
import model.MonHoc;
import model.SinhVien;

/**
 *
 * @author Cao Sy Hai Long
 */
public class DataController {
    private String hostName;
    private String dbName;
    private String userName;
    private String password;
    private Connection con;

    
    public DataController() throws SQLException {
        this.hostName = "localhost";
        this.dbName = "quanlydiemptit";
        this.userName = "root";
        this.password = "jkluio789";
        getMySQLConnection();
    }

    
    public DataController(String hostName, String dbName, String userName, String password) throws SQLException {
        this.hostName = hostName;
        this.dbName = dbName;
        this.userName = userName;
        this.password = password;
        getMySQLConnection();
    }
    
    // Khởi tạo kết nối tới CSDL của MySQL
    public void getMySQLConnection() throws SQLException {
        String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?useSSL=false&allowPublicKeyRetrieval=true";
        con = DriverManager.getConnection(connectionURL, userName, password);
    }
    
    public void closeMySQLConnection() throws SQLException {
        if(con != null)
            con.close();
    }
    
    // Lấy thông tin lớp học bằng mã lớp
    public Lop getLopByMaLop(String maLop) throws SQLException {
        String sql = "select *\n" +
                     "from lop\n" +
                     "where MaLop = \"" +  maLop + "\"";
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        if(rs.next())
            return new Lop(rs.getString("MaLop"), rs.getInt("Nhom"), rs.getInt("SoLuong"), rs.getInt("KipHoc"), rs.getString("Thu"));
        else
            return null;
    }
    
    // Lấy thông tin môn học bằng mã môn
    public MonHoc getMonHocByMaMon(String maMon) throws SQLException {
        String sql = "select *\n" +
                     "from monhoc\n" +
                     "where MaMon = \"" +  maMon + "\"";
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        if(rs.next())
            return new MonHoc(rs.getString("MaMon"), rs.getString("TenMon"));
        else
            return null;
    }
    
    // Lấy danh sách thông tin môn học bằng mã giảng viên
    public ArrayList<MonHoc> getMonHocByMaGV(String ID) throws SQLException {
        ArrayList<MonHoc> cacMonHoc = new ArrayList<>();
        String sql = "select *\n" +
                     "from monhoc\n" +
                     "where MaMon in (select MonHocMaMon\n" +
                     "from giangvienmonhoc\n" +
                     "where GiangVienMaGV = " + "\"" + ID + "\");";
        System.out.println("where GiangVienMaGV = " + "\"" + ID + "\");");
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        while(rs.next()) {
            cacMonHoc.add(new MonHoc(rs.getString(1), rs.getString(2)));
        }
        con.close();
        return cacMonHoc;
    }
    
    
    // Lấy danh sách thông tin các lớp bằng mã môn
    public ArrayList<Lop> getLopByMaMon(String maMon) throws SQLException {
        Statement statement = con.createStatement();
        String sql = "select *\n" +
                     "from lop\n" +
                     "where KiHocMonHocID in (select ID\n" +
                     "from kihocmonhoc\n" +
                     "where MonHocMaMon = \"" + maMon + "\")";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<Lop> cacLop = new ArrayList<>();
        while(rs.next()) {
            Lop lop = new Lop(rs.getString(1), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7));
            cacLop.add(lop);
        }
        con.close();
        return cacLop;
    }
    
    // Lấy danh sách sinh viên bằng mã lớp
    public ArrayList<SinhVien> getSinhVienByMaLop(String maLop) throws SQLException {
        Statement statement = con.createStatement();
        String sql = "select *\n" +
                     "from sinhvien\n" +
                     "where MaSV in (select SinhVienMaSV\n" +
                     "               from sinhvien_lop\n" +
                     "               where LopMaLop = \"" + maLop + "\")";
        ResultSet rs = statement.executeQuery(sql);
        ArrayList<SinhVien> dsSinhVien = new ArrayList<>();
        while(rs.next()) {
            SinhVien sv = new SinhVien(rs.getString(1), rs.getString(2), rs.getString(3), rs.getDate(4));
            dsSinhVien.add(sv);
        }
        statement.close();
        return dsSinhVien;
        
    }
    
    // Lấy thông tin điểm của sinh viên bằng mã sinh viên và mã môn
    public Diem getDiemByMaSV_MaMon(String maSV, String maMon) throws SQLException {
        String sql = "select *\n" +
                     "from diem\n" +
                     "where SinhVienMaSV = \"" + maSV + "\" and MonHocMaMon = \"" + maMon + "\"";
        Statement statement = con.createStatement();
        ResultSet rs = statement.executeQuery(sql);
        if(rs.next())
            return new Diem(rs.getInt(1), rs.getFloat(4), rs.getFloat(5), rs.getFloat(6), rs.getFloat(7), rs.getFloat(8));
        return new Diem();
    }

    public void addDiem(Diem diem) throws SQLException {
        float diemCC = diem.getDiemCC();
        float diemKT = diem.getDiemKT();
        float diemTN_TH = diem.getDiemTN_TH();
        float diemCuoiKi = diem.getDiemCuoiKi();
        float diemTrungBinh = diem.getDiemTrungBinh();
        String sql = "insert into diem (MonHocMaMon, SinhVienMaSV, DiemCC, DiemKT, DiemTN_TH, DiemCuoiKi, DiemTrungBinh)\n" +
                "values  ('" + diem._monHoc + "', '" + diem._sinhVien + "', " + diemCC + ", " + diemKT + ", " + diemTN_TH + ", " + diemCuoiKi + ", " + diemTrungBinh + ")";
        Statement statement = con.createStatement();
        statement.executeUpdate(sql);
    }

    public void updateDiem(Diem diem) {
        try {
            float diemCC = diem.getDiemCC();
            float diemKT = diem.getDiemKT();
            float diemTN_TH = diem.getDiemTN_TH();
            float diemCuoiKi = diem.getDiemCuoiKi();
            float diemTrungBinh = diem.getDiemTrungBinh();
            String sql = "update diem\n" +
                    "set DiemCC = " + diemCC + ", DiemKT = " + diemKT + ", DiemTN_TH = " + diemTN_TH + ", DiemCuoiKi = " + diemCuoiKi + ", DiemTrungBinh = " + diemTrungBinh + "\n" +
                    "where ID = " +  diem.getID();
            Statement statement = con.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DataController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
