/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import controller.DataController;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Diem;
import model.SinhVien;

/**
 *
 * @author Cao Sy Hai Long <your.name at your.org>
 */
public class LuuDiemLopServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        HttpSession session = request.getSession();
        ArrayList<SinhVien> dsSinhVien = (ArrayList<SinhVien>) session.getAttribute("danhSachSV");
        ArrayList<Diem> dsDiem = (ArrayList<Diem>) session.getAttribute("danhSachDiem");
        System.out.println("Ma sinh vien: " + dsSinhVien.get(16).getMaSV());
        System.out.println("Diem trung binh: " + dsDiem.get(16).getDiemTrungBinh());
        DataController control = new DataController();
        for(int i = 0; i < dsSinhVien.size(); i++) {
            String maSV = dsSinhVien.get(i).getMaSV();
            String maMon = request.getParameter("maMon");
            Diem diem = control.getDiemByMaSV_MaMon(maSV, maMon);
            int diemID = diem.getID();
            if(diemID == -1) {
                diem = dsDiem.get(i);
                diem._monHoc = maMon;
                diem._sinhVien = maSV;
                control.addDiem(dsDiem.get(i));
            }
            else {
                diem = dsDiem.get(i);
                diem.setID(diemID);
                diem._monHoc = maMon;
                diem._sinhVien = maSV;
                control.updateDiem(diem);
            }
        }
        control.closeMySQLConnection();

        request.setAttribute("daLuu", Boolean.TRUE);
        String url = "/GDdiem.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LuuDiemLopServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(LuuDiemLopServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
