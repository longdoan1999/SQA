<%-- 
    Document   : GDdanhsachSV
    Created on : Apr 14, 2021, 8:57:47 PM
    Author     : Cao Sy Hai Long
--%>

<%@page import="model.MonHoc"%>
<%@page import="java.util.ArrayList"%>
<%@page import="controller.DataController"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1 style="text-align: center">Chọn môn học</h1>
        <p>Chọn môn học trong danh sách</p>
        
        <form action = "GDchonLop.jsp" method="get">
            <table>
                <%
                    String accountID = request.getAttribute("ID").toString();
                    DataController control = new DataController();
                    ArrayList<MonHoc> cacMonHoc = control.getMonHocByMaGV(accountID);
                    System.out.println(cacMonHoc.size());
                    for(int i = 0; i < cacMonHoc.size(); i++) {
                        String mon = cacMonHoc.get(i).getTenMon();
                        String maMon = cacMonHoc.get(i).getMaMon();
                        System.out.println(mon);
                        String btID = "bt" + (i + 1);
                        System.out.println(btID);
                        
                %>
                        <tr>
                            <td style="text-align: center">
                                <label for=<%= btID %>><%= mon %></label>
                            </td>
                            <td>
                                <button name="MonHoc" type="Submit" value= <%= maMon %> id= <%= btID %>>Chọn</button>
                            </td>
                        </tr>
                <%
                    }
                %>

            </table>
        </form>
    </body>
</html>
